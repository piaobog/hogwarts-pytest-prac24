"""
__author__ = '霍格沃兹测试开发学社'
__desc__ = '更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860'
"""
from time import sleep
# pip install pyyaml
import yaml
from selenium import webdriver

"""登录"""


# 帐号会互顶！！！！
class TestCookieLogin:
    def setup_class(self):
        # 前置动作
        # 准备资源文件，做初始化
        # 创建一个driver实例变量
        self.driver = webdriver.Chrome()
        self.driver.maximize_window()

    def teardown_class(self):
        # 后置处理
        # self.driver.quit()
        pass

    def test_get_cookie(self):
        """获取cookie"""
        # 1、访问企业微信登录
        self.driver.get("https://work.weixin.qq.com/wework_admin/loginpage_wx?from=myhome")
        # 2、扫码，手动登录 --- 手机端有时候需要点两次
        sleep(20)
        # 3、登录后获取cookie
        cookies = self.driver.get_cookies()
        # 4、保存cookie
        with open("../datas/cookies.yaml", "w") as f:
            # python 对象转成yaml 格式
            yaml.safe_dump(data=cookies, stream=f)

    def test_add_cookie(self):
        """植入cookie"""
        # 1、访问企业微信主页
        self.driver.get("https://work.weixin.qq.com/wework_admin/frame")
        # 2、获取本地的cookies
        with open("../datas/cookies.yaml") as f:
            # yaml 格式 转成python 对象
            cookies = yaml.safe_load(f)

        # 3、植入cookies
        for cookie in cookies:
            # cookie 就是一个字典
            self.driver.add_cookie(cookie)
        # 4、再次访问企业微信主页/ 刷新页面
        self.driver.get("https://work.weixin.qq.com/wework_admin/frame")
        sleep(3)
