"""
__author__ = '霍格沃兹测试开发学社'
__desc__ = '更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860'
"""
from time import sleep

import yaml
from selenium import webdriver
from selenium.webdriver.common.by import By


class TestAddMemeber:
    """添加成员"""

    def setup_class(self):
        # 前置动作

        # 准备资源文件，做初始化
        # 第一步：创建一个driver实例变量
        self.driver = webdriver.Chrome()
        self.driver.maximize_window()
        self.driver.implicitly_wait(5)

        """第二步：植入cookie完成登录"""
        # 1、访问企业微信主页
        self.driver.get("https://work.weixin.qq.com/wework_admin/frame")
        # 2、获取本地的cookies
        with open("../datas/cookies.yaml") as f:
            # yaml 格式 转成python 对象
            cookies = yaml.safe_load(f)

        # 3、植入cookies
        for cookie in cookies:
            # cookie 就是一个字典
            self.driver.add_cookie(cookie)
        # 4、再次访问企业微信主页/ 刷新页面
        self.driver.get("https://work.weixin.qq.com/wework_admin/frame")

        # mock 测试数据
        from faker import Faker
        fake = Faker('zh_CN')
        self.name = fake.name()
        self.phone_number = fake.phone_number()
        self.accid = fake.ssn()

    def teardown_class(self):
        # 后置处理
        # 不要退出 ！！！防止互踢
        # self.driver.quit()
        pass

    def test_addmember(self):
        # name = "hogwarts001"
        # accid = "1111111"
        # phonenum = "13000000000"
        # 功能：添加通讯录成员
        # 1、首页点击添加成员
        # 第一种写法 ------- 可以
        self.driver.find_element(By.CSS_SELECTOR, ".index_service_cnt_item_title").click()
        # 第二种写法  ------ 可以
        # self.driver.find_elements(By.CSS_SELECTOR, ".index_service_cnt_item_title")[0].click()
        # 2、输入姓名 帐号，手机号
        self.driver.find_element(By.ID, "username").send_keys(self.name)
        self.driver.find_element(By.ID, "memberAdd_acctid").send_keys(self.accid)
        self.driver.find_element(By.ID, "memberAdd_phone").send_keys(self.phone_number)

        # 3、点击保存
        self.driver.find_element(By.CSS_SELECTOR, ".js_btn_save").click()
        # 4、验证添加成功
        result = self.driver.find_element(By.ID, "js_tips").text
        # value = self.driver.find_element().get_attribute("data-usercardinit")
        assert "保存成功" == result
        # sleep(5)
